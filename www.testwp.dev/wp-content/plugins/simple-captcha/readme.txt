=== Simple CAPTCHA ===
Contributors: keypic, Zorex
Tags: captcha, image, security, simple captcha
Requires at least: 3.0
Tested up to: 3.6.1
Stable tag: 1.3.10
License: GPLv2 or later

NO CAPTCHA Anti-Spam. Keypic checks your blogs comments against the comprehensive Keypic Web Service to see if they look like spam or not.

== Description ==
Keypic is not a CAPTCHA provider, we completely remove the concept of CAPTCHA!
Keypic checks your forms against the comprehensive Keypic Web Service to see if they look like spam or not and lets you
review the spam it catches under your blog's "Comments" admin screen.
Keypic also protects your login form from focused hacking attempts, your registration form from unwanted spammers and most other forms that you insert into your website :)
If you want a complete free antispam solution and you don't want to install CAPTCHAs plugins or plugins similar to CAPTCHA, this is the right choice for you.
It is also possible to perform a live installation from http://wordpress.keypic.com/


Major new features in Keypic 1.3 include: 

For Everyone: 
Registration is no longer needed for Keypic to function, but without registration some limitations apply.
Activate single forms independently from each other.
Keypic includes the Contact Form 7 extension in the download.
Check your FormID in Keypic Configuration.
If your web host is unable to reach Keypic's servers, the plugin will automatically retry until the connection is restored.
PS: You'll need an Keypic FormID to get improved functionality.
Keys are free for every kind of use (It's free and always will be.)

Registered Users: 
Access to comment status history; Easily see which comments were caught or cleared by Keypic, and which were spammed or unspammed by a moderator 
Access to user status history, so you can easily see which users were caught or cleared by Keypic. 

== Installation ==

Upload the Keypic plugin to your blog, Activate it, then [get registered on Keypic website](http://keypic.com/modules/register/) (if you want it, is not obbligatory) and  [get a new FormID](http://keypic.com/modules/forms/).

== Frequently Asked Questions ==
Ask some questions and we will be happy to answer!
Just bring us a [feedback](http://keypic.com/modules/feedback/)

== Screenshots ==

1. How Keypic act on comments form.
2. How Keypic act on login form.
3. How Keypic act on register form.
4. How Keypic act on lastpassword.
5. How Keypic act on register form in case of spam.

== Upgrade Notice ==
A new version is waiting for next week

== Changelog ==

= 1.3.10 =
* Bug Fix: Minor bug fixed

= 1.3.9 =
* Bug Fix: Minor bug fixed

= 1.3.8 =
* Bug Fix: Minor bug fixed

= 1.3.7 =
* Bug Fix: Minor bug fixed

= 1.3.6 =
* Bug Fix: Minor bug fixed

= 1.3.5 =
* Bug Fix: Minor bug fixed